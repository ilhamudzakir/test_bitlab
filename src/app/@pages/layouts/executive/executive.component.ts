import { Component, OnInit,ViewEncapsulation } from '@angular/core';
import { RootLayout } from '../root/root.component';
declare var pg: any;
@Component({
  selector: 'executive-layout',
  templateUrl: './executive.component.html',
  styleUrls: ['./executive.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class ExecutiveLayout extends RootLayout implements OnInit {
  menuLinks = [
    {
      label:"Dashboard",
      //details:"12 New Updates",
      routerLink:"/dashboard",
      iconType:"pg",
      iconName:"home",
    },
    {
        label:"Employee",
        routerLink:"/employee",
        iconType:"pg",
        iconName:"employee"
    },
    {
      label:"Payroll",
      routerLink:"/payroll",
      iconType:"pg",
      iconName:"payroll"
    },
    {
      label:"Settings",
      iconType:"letter",
      iconName:"Settings",
      toggle:"close",
      mToggle:"close",
      submenu:[
        {
          label:"Organization",
          routerLink:"/settings/org",
          iconType:"letter",
          iconName:"Organization",
        },
        {
          label:"User Access",
          routerLink:"/user/access",
          iconType:"letter",
          iconName:"UA",
        },
        {
          label:"Job",
          routerLink:"/settings/job",
          iconType:"letter",
          iconName:"Job",
        },
        {
          label:"Job Class",
          routerLink:"/settings/job_class",
          iconType:"letter",
          iconName:"Job Class",
        },
        {
          label:"Area",
          routerLink:"/settings/area",
          iconType:"letter",
          iconName:"Area",
        },
        {
          label:"Cost Center",
          routerLink:"/settings/cost",
          iconType:"letter",
          iconName:"Cost Center",
        },
        {
          label:"Account Number",
          routerLink:"/settings/acc",
          iconType:"letter",
          iconName:"Account Number",
        }
      ]
    },
  ]
  ngOnInit() {
    pg.isHorizontalLayout = true;
    this.changeLayout("horizontal-menu");
    this.changeLayout("horizontal-app-menu");
    this.checklogin();
    
  }

  logout(){
    this.logouts();
  }
}
