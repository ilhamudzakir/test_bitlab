import { Component, OnInit, OnDestroy, ViewChild, HostListener, AfterViewInit,Input,ViewEncapsulation } from '@angular/core';
import { RootLayout } from '../root/root.component';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'condensed-layout',
  templateUrl: './condensed.component.html',
  styleUrls: ['./condensed.component.scss'],
  encapsulation: ViewEncapsulation.None,
})

export class CondensedComponent extends RootLayout implements OnInit {
  adminSession;


    logout(){
      this.logouts();
    }

    ngOnInit() {
       this.adminSession =this.adminsession();
       console.log(this.adminSession);
    }
      
}
