import { Component, OnInit, ViewChild } from '@angular/core';
import {NgForm} from '@angular/forms';
import { RouterModule, Router } from '@angular/router';
import { HttpClient, HttpErrorResponse, HttpHeaders,HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { CookieService} from 'ngx-cookie-service';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { MessageService } from '../../@pages/components/message/message.service';
import { JwtHelperService } from "@auth0/angular-jwt";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  constructor( private router: Router, private http : HttpClient, private CookieService: CookieService, private _notification: MessageService) { 

  }
  @ViewChild('resetAppModal') resetAppModal: ModalDirective;
  appEmail : string = '';
  image;
  celcius;
  tmp_min;
  tmp_max;
  weather=[];
  ngOnInit() {

  }
  onSubmit(loginForm: NgForm) {
    let headers = new HttpHeaders();
    
    if(loginForm.value.email!='' || loginForm.value.email!=undefined){
      const url = 'http://api.openweathermap.org/data/2.5/weather';
      let httpOptions = {
        headers: new HttpHeaders({ 
          'Accept' : '*/*',
          'Access-Control-Allow-Origin':'*',
          'Content-Type': 'application/x-www-form-urlencoded',
          }),
          params: new HttpParams()
                .set("q", loginForm.value.email)
                .set("appid", '9281ce9fa6072cbd884ecde4ebd1cb3c')
      };
      return this.http.get(url,httpOptions)
      .subscribe((data)=>{
        console.log(data);
        if(data['coord']){
          this.weather=[];
          this.weather.push(data['weather']);
          document.getElementById("image-icon").setAttribute('src','http://openweathermap.org/img/w/'+ data['weather'][0]['icon']+'.png')
          document.getElementById('weither').classList.remove("hide");
          document.getElementById('cuaca').innerHTML=data['weather'][0]['main'];
          document.getElementById('location').innerHTML=data['name'];
          document.getElementById('country').innerHTML=data['sys']['country'];
          document.getElementById('parameter-max').innerHTML=data['main']['temp_max'];
          document.getElementById('parameter-min').innerHTML=data['main']['temp_min'];
          document.getElementById('temp-main').innerHTML=data['main']['temp_min'];

        }
        else{
          var element = document.getElementById("notif");
        element.innerHTML='City is not found';
          element.classList.remove("hide");
        }
      }, (data)=>{
        console.log(data);
        var element = document.getElementById("notif");
        element.innerHTML='City is not found';
        element.classList.remove("hide");
      });
    }
    
  }
  resetShowModal(){
    this.resetAppModal.show();
  }

}
