import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SessionRoute } from './session.routing';

import { SharedModule } from '../@pages/components/shared.module';
import { ErrorComponent } from './error/error.component';
import { LoginComponent } from './login/login.component';
import { LockscreenComponent } from './lockscreen/lockscreen.component';
import { RegisterPageComponent } from './register/register.component';
import { CarouselModule } from 'ngx-bootstrap';
import { ModalModule } from 'ngx-bootstrap';

@NgModule({
  imports: [
    CommonModule,
    FormsModule, ReactiveFormsModule, 
    SharedModule,
    CarouselModule,
    RouterModule.forChild(SessionRoute),
    ModalModule.forRoot(),
  ],
  declarations: [ErrorComponent,LoginComponent,LockscreenComponent, RegisterPageComponent]
})
export class SessionModule { }
