import { Component, OnInit } from '@angular/core';
import { ViewEncapsulation } from '@angular/core'
import { MessageService } from '../../@pages/components/message/message.service';

import {Observable,of, from } from 'rxjs';


import { HttpClient, HttpErrorResponse, HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-lockscreen',
  encapsulation: ViewEncapsulation.None,
  templateUrl: './lockscreen.component.html',
  styleUrls: ['./lockscreen.component.scss']
})
export class LockscreenComponent implements OnInit {

  constructor(private http: HttpClient, private _notification: MessageService) { }
  arrContent = [];
  arrContentTwo = [];
  arrContentThree = [];
  dataRoom;
  slides = [];
  countBook;
  today = new Date();
  time = this.today.getHours() + ":" + (this.today.getMinutes() < 10 ? '0' : '') +this.today.getMinutes();
  day = this.today.toLocaleString('en-us', { weekday: 'long' }) + ', ' + this.today.getDate() + ' ' + this.today.toLocaleString('en-us', { month: 'long' }) + ' ' + this.today.getFullYear();
  ngOnInit() {
    this.getData()
    console.log(this.slides);
    var slides = this.slides;
    setInterval(function () {
      this.today = new Date();
      this.time = this.today.getHours() + ":" + (this.today.getMinutes() < 10 ? '0' : '') +this.today.getMinutes();
      this.day = this.day = this.today.toLocaleString('en-us', { weekday: 'long' }) + ', ' + this.today.getDate() + ' ' + this.today.toLocaleString('en-us', { month: 'long' }) + ' ' + this.today.getFullYear();
      document.getElementById("time").innerHTML = this.time;
      document.getElementById("day").innerHTML = this.day;
    }, 60 * 1000);
    
    setInterval( () => {
      this.refreshing();
   }, 5000);
    function myLoop(i, x, time) {

      setTimeout(() => {
        document.getElementById("detail-room-" + i + "-" + x).classList.add("fadeout");
        document.getElementById("detail-room-" + i + "-" + x).classList.remove("now");
        document.getElementById("detail-room-" + i + "-" + x).style.display = "none";
        slides[i].push(slides[i][x])
        slides.slice(slides[i][x]);
        var a = x + 1;
        document.getElementById("detail-room-" + i + "-" + a).classList.add("fadein");
        document.getElementById("detail-room-" + i + "-" + a).classList.remove("comming");
        document.getElementById("detail-room-" + i + "-" + a).classList.add("now");
      }, time)


    }

    var xi = 0;
    var roomnum = 0;
    let start = 10000;
    setTimeout(() => {
      for (let i = 1; i <= 1000; i++) {
        slides.forEach(datas => {
          start = start + 1000;
          if (this.arrContentTwo[roomnum] > 1) {
            myLoop(roomnum, this.arrContentThree[roomnum], start);
            this.arrContentThree[roomnum] = this.arrContentThree[roomnum] + 1;
            //console.log("add room"+roomnum,this.arrContentThree[roomnum]);
            // if(this.arrContentThree[roomnum]==this.arrContentTwo[roomnum]){
            //   this.arrContentThree[roomnum]=0;
            // }
          }
          this.slides = slides;
          roomnum++;
        });

        xi++;
        roomnum = 0;
        start = 1000 * this.arrContentThree.length + start;
      }
    }, 3000);
  }

  refreshing(){
    this.http.get(environment.app_url_v1 + 'booking/digital').subscribe((datay) => {
      if (datay['status'] == 'success') {
        if (datay['count'] != this.countBook) {
          window.location.reload();
        }
      }
    });
  }

  getData() {
    let httpOptions = {
      headers: new HttpHeaders({
        'Access-Control-Allow-Origin': '*',
        'Content-Type': 'application/json',
        'Access-Control-Allow-Headers': 'Content-Type',
        'Access-Control-Allow-Credentials': 'true',
      })
    };
    const url = environment.app_url_v1 + 'booking/digital';
    this.slides=[];
    this.arrContent=[];
    this.arrContentTwo=[];
    this.arrContentThree=[];
    this.http.get(url, httpOptions).toPromise().then(data => {
      if (data['status'] == 'success') {
        this.countBook = data['count']
        this.dataRoom = data
        data['response'].forEach(data => {
          var dataBooking = []
          data.booking.forEach(booking => {
            dataBooking.push(booking);
          });
          this.slides.push(dataBooking);
          this.arrContent.push(dataBooking);

          this.arrContentTwo.push(dataBooking.length);
          this.arrContentThree.push(0);
        });
      }
    });

  }


}