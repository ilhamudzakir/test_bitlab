import { Component, OnInit,ViewChild,ViewEncapsulation } from '@angular/core';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {NgForm} from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { CookieService } from 'ngx-cookie-service';
import { MessageService } from '../../@pages/components/message/message.service';
import { NgxChartsModule } from '@swimlane/ngx-charts';
import { HttpClient, HttpErrorResponse, HttpHeaders,HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { CalendarEvent, CalendarMonthViewDay } from 'angular-calendar';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class CondensedDashboardComponent implements OnInit {
  scrollBarHorizontal = (window.innerWidth < 960);
  columnModeSetting = (window.innerWidth < 960) ? 'standard':'force';

  constructor(private CookieService: CookieService,private http : HttpClient, private _notification: MessageService) {
    window.onresize = () => {
      this.scrollBarHorizontal = (window.innerWidth < 960);
      this.columnModeSetting = (window.innerWidth < 960) ? 'standard':'force';
    };
  }

  ngOnInit() {
    this.getdata();
  }
  ngAfterViewInit(){
  }
  getdata(){
    const url = 'http://api.openweathermap.org/data/2.5/weather?q=jakarta&appid=9281ce9fa6072cbd884ecde4ebd1cb3c';
    this.http.get(url).subscribe((data)=>{
      
    });
  }
 }
