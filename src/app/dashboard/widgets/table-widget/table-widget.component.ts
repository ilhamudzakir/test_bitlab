import { Component, OnInit,ViewChild,ViewEncapsulation } from '@angular/core';
import {DatatableComponent} from '@swimlane/ngx-datatable';
import {NgForm} from '@angular/forms';
import { ModalDirective } from 'ngx-bootstrap/modal';
import { CookieService } from 'ngx-cookie-service';
import { MessageService } from '../../../@pages/components/message/message.service';

import { HttpClient, HttpErrorResponse, HttpHeaders,HttpParams } from '@angular/common/http';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'table-widget',
  templateUrl: './table-widget.component.html',
  styleUrls: ['./table-widget.component.scss'],
  encapsulation:ViewEncapsulation.None
})
export class TableWidgetComponent implements OnInit {

  countMostActive = [];
  scrollBarHorizontal = (window.innerWidth < 960);
  columnModeSetting = (window.innerWidth < 960) ? 'standard':'force';

  constructor(private CookieService: CookieService,private http : HttpClient, private _notification: MessageService) {
    window.onresize = () => {
      this.scrollBarHorizontal = (window.innerWidth < 960);
      this.columnModeSetting = (window.innerWidth < 960) ? 'standard':'force';
    };
  }

  ngOnInit() {
    this.getDataMostActive();
  }

  getDataMostActive(){
    const url = environment.app_url_v1+'additional/most-active';
    this.http.get(url).subscribe((data)=>{
      // console.log(data);
      if(data['status']=='success'){
        var response=[];
        data['response'].forEach(element => {
          response.push(element);
        });
        this.countMostActive = response;
      }
    });
  }

}
