import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Observable} from 'rxjs';
import { CookieService } from 'ngx-cookie-service';

@Injectable({
  providedIn: 'root'
})
export class CustomHttpInterceptorService implements HttpInterceptor {
  constructor(private CookieService: CookieService) { }
  
  
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    var adminSession : any = [];
  //  if(this.CookieService.get('admin')){
  //     //adminSession=JSON.parse(this.CookieService.get('admin'));
  //     adminSession = {
  //       data:{
  //         token:'13a15d6d82a73178b92cae63b468a4f16d12c9733e2095529fa86e6104d8c727'
  //       }
  //     };
  //   }else{
  //     adminSession = {
  //       data:{
  //         token:'13a15d6d82a73178b92cae63b468a4f16d12c9733e2095529fa86e6104d8c727'
  //       }
  //     };
  //   }
    request = request.clone({headers: request.headers.set('Access-Control-Allow-Origin', '*')});
    if(!request.headers.has('No-Content-Type')){
      if (!request.headers.has('Content-Type')) {
        request = request.clone({headers: request.headers.set('Content-Type', 'application/json')});
      }
    }
    request = request.clone({headers: request.headers.set('Access-Control-Allow-Headers', 'Content-Type')});
    request = request.clone({headers: request.headers.set('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT')});
    request =request.clone({headers: request.headers.set("Access-Control-Allow-Credentials", "true")});
    request= request.clone({headers:request.headers.set("Set-Cookie", "HttpOnly;Secure;SameSite=Strict")});
    //request = request.clone({headers: request.headers.set('Authorization', 'Bearer '+adminSession.data.token)});
    if (!request.headers.has('Accept')) {
      request = request.clone({headers: request.headers.set('Accept', '*/*')});
    }
    return next.handle(request);

    // request = request.clone({headers: request.headers.set('Access-Control-Allow-Origin', '*')});
    // request = request.clone({headers: request.headers.set('Content-Type', 'application/json')});
    // request = request.clone({headers: request.headers.set('Access-Control-Allow-Headers', 'Content-Type')});
    // request = request.clone({headers: request.headers.set('Set-Cookie', 'cross-site-cookie=name; SameSite=None; Secure')});
    // request = request.clone({headers: request.headers.set('Authorization', 'Bearer e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855')});
    // if (!request.headers.has('Accept')) {
    //   request = request.clone({headers: request.headers.set('Accept', 'application/json')});
    // }
    // return next.handle(request);
 
  }
  
}
