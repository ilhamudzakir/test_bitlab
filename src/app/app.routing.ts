import { Routes } from '@angular/router';
//Layouts
import { 
  CondensedComponent,
  ExecutiveLayout,
  CorporateLayout,
  BlankComponent,
  
} from './@pages/layouts';

//Sample Pages
import { CondensedDashboardComponent} from './dashboard/condensed/dashboard.component';
import { ErrorComponent} from './session/error/error.component';




export const AppRoutes: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '404', redirectTo: 'session/error/Not Found', pathMatch: 'full' },
  
  {
    path: '',
    component: CondensedComponent,
    children: [{
      path: 'home',
      component: CondensedDashboardComponent
    }],
  },
  
];
